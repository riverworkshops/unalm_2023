<!-- #region -->
# Charla: Nuevas herramientas de teledetección aplicadas a la caracterización morfodinamica de ríos. UNALM 2023

Este repo cuenta con toda la información respecto a la charla **"Nuevas herramientas de teledetección aplicadas a la caracterización morfodinamica de ríos"**. 
Cualquier duda a ldominguezruben@gmail.com o ldominguez@fich.unl.edu.ar


## Sobre la charla

El charla  "Nuevas herramientas de teledetección aplicadas a la caracterización morfodinamica de ríos", pretende realizar un barrido de conceptos sobre las nuevas herramientas necesarias para el analisis de patrones morfometricos en grandes sistemas fluviales. Los grandes tópicos que se discutiran serán:
- Aplicaciónes
- Descripción de parámetros básicos de morfometría fluvial
- Implementación del software [MStaT](https://meanderstatistics.blogspot.com/p/download.html)
 

### Sobre el disertante
El curso se desarrollará con el dictado de los docentes:
- [Lucas Dominguez Ruben](https://www.researchgate.net/profile/Lucas-Dominguez-Ruben-3)


### Dependencias 
Las siguientes son dependencias de Python necesarias a instalar para las tareas del curso:
- [Numpy](https://numpy.org/)
- [Scikit learn](https://scikit-learn.org/stable/index.html)
- [Matplotlib](https://matplotlib.org/)
- [Pandas](https://pandas.pydata.org/)
- [Scipy](https://scipy.org/)
- [GDAL](https://gdal.org/api/python.html)
- [Rasterio](https://rasterio.readthedocs.io/en/latest/quickstart.html)

#### Como descargar este repositorio???
Debemos ir a una terminal (Anaconda prompt, Jupyter-notebook o Jupyter.lab), en la misma ubicarnos en la carpeta donde queremos (usamos el comando ```cd direccion_de_carpeta```) descargar la información y luego realizamos el siguiente comando:
```
git clone https://gitlab.com/riverworkshops/unalm_2023.git
```
<!-- #endregion -->

```python

```
